import React from "react";
import { useParams } from "react-router-dom";
// material style
import Box from "@mui/material/Box";
import TextField from "@mui/material/TextField";
import Stack from "@mui/material/Stack";
import Button from "@mui/material/Button";

const Addlist = ({ add }) => {
  console.log("Add Data:", add);
  //click on submit
  const Submit = (e) => {
    e.preventDefault();
    const { fname, lname, phone, email } = e.target.elements;
    console.log(fname.value);
    console.log(lname.value);
    console.log(phone.value);
    console.log(email.value);
    //find params
    if (Params.id) {
      let tempadd = [...add.prop1];
      tempadd = tempadd.map(function (obj) {
        if (obj.id === Params.id) {
          obj = {
            id: Params.id,
            fname: fname.value,
            lname: lname.value,
            phone: phone.value,
            email: email.value,
          };
        }
        return obj;
      });
      add.prop2(tempadd);
    } else {
      add.prop2([
        ...add.prop1,
        {
          id: new Date().toString(),
          fname: fname.value,
          lname: lname.value,
          phone: phone.value,
          email: email.value,
        },
      ]);
    }
    // event for clear current input
    e.currentTarget.reset();
  };
  // use Params 
  const Params = useParams();
  console.log(Params);
  const tempadd = add.prop1.find(function(obj){
    return obj.id === Params.id;
  })
  return (
    <>
      <form onSubmit={Submit}>
        <Box
          sx={{
            display: "flex",
            alignItems: "center",
            "& > :not(style)": { m: 1 },
          }}
        >
          <TextField
            helperText=" "
            id="demo-helper-text-aligned-no-helper"
            label="First Name"
            name="fname"
            defaultValue={tempadd?.fname}
          />
          <TextField
            helperText=" "
            id="demo-helper-text-aligned-no-helper"
            label="Last Name"
            name="lname"
            defaultValue={tempadd?.lname}
          />
        </Box>
        <Box
          sx={{
            display: "flex",
            alignItems: "center",
            "& > :not(style)": { m: 1 },
          }}
        >
          <TextField
            helperText=" "
            id="demo-helper-text-aligned-no-helper"
            label="Phone Number"
            name="phone"
            defaultValue={tempadd?.phone}
          />
          <TextField
            helperText=" "
            id="demo-helper-text-aligned-no-helper"
            label="Email Address"
            name="email"
            defaultValue={tempadd?.email}
          />
        </Box>
        <Stack spacing={2} direction="row">
          <Button
            variant="text"
            type="submit"
            style={{
              textDecoration: "none",
              color: "white",
              backgroundColor: "#ff5722",
              padding: "1rem",
              marginLeft: "10px",
            }}
            
          >
            Submit
          </Button>
        </Stack>
      </form>
    </>
  );
};

export default Addlist;
