import React, {useState} from "react";
import {BrowserRouter as Router} from "react-router-dom";
import Routes from "./routes";


function App() {
// Declare global state
const [globalState, setGlobalState] = useState([]);
console.log('globalState:',globalState);


  return (
   <Router>
     <Routes prop1={globalState} prop2={setGlobalState} />
   </Router>
  );
}

export default App;
